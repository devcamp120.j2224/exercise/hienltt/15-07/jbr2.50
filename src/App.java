import com.devcamp.Account;
import com.devcamp.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        
        Customer customer1 = new Customer(001, "Nguyen Van A", 10);
        Customer customer2 = new Customer(002, "Tran Van B", 15);
        System.out.println(customer1);
        System.out.println(customer2);

        Account account1 = new Account(999, customer1, 456.789);
        Account account2 = new Account(777, customer2, 123.987);
        System.out.println(account1);
        System.out.println(account2);
    }
}

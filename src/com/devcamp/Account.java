package com.devcamp;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    public Account(int id, Customer customer, double balance){
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public Account(int id, Customer customer){
        this.id = id;
        this.customer = customer;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getCustomerName(){
        return this.customer.getName();
    }
    public Account deposit(double amount){
        this.balance += amount;
        return this;
    }
    public Account wihtdraw(double amount){
        if(this.balance >= amount){
            this.balance -= amount;
        } else {
            System.out.println("amount withdraw exeeded the current balance!");
        }
        return this;
    }
    @Override
    public String toString(){
        double roundOff = (double) Math.round(balance*100)/100; //làm tròn đến 2 chứ số thập phân
        return "'" + this.customer.getName() + "(" + this.id +  ") balance= " + roundOff +"'";
    }
}
